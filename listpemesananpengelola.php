<?php
 require_once('dbConnect.php');

 $result = array();

 $idpelanggan = $_GET['iduser'];

 $sql = "SELECT *,pemesanan.jumlah_ruang,DATE(tanggal_pemesanan) as hari_pemesanan,DATE(tanggal_akhirPemesanan) as hari_Akhirpemesanan FROM pemesanan inner join lokasi on pemesanan.id_lokasi = lokasi.id_lokasi inner join user on pemesanan.id_pemilik = user.id_user WHERE pemesanan.id_pemilik='$idpelanggan' order by id_pemesanan desc";
 
 $res = mysqli_query($con,$sql);
	
 while($row = mysqli_fetch_array($res)){
	$tanggalawal = strtotime($row['tanggal_pemesanan']);
  	$tanggalakhir = strtotime($row['tanggal_akhirPemesanan']);
	$timeDiff = abs($tanggalakhir - $tanggalawal);

	$numberDays = $timeDiff/86400;
	
	$numberDays = intval($numberDays);
 
 	$statuspemesanan = $row['konfirmasi1'].$row['konfirmasi2'].$row['konfirmasi3'];
 
 	array_push($result,array('namapemilik'=>$row['nama'],'jumlahharga'=>$row['jumlah_harga'],'namalokasi'=>$row['nama_lokasi'],'tipelokasi'=>$row['tipe'],'hargalokasi'=>$row['harga'],'nomorpemesanan'=>$row['id_pemesanan'],'lamabooking'=>$numberDays,'hargalokasi'=>$row['harga'],'jumlahruangan'=>$row['jumlah_ruang'],'diskon'=>"0",'statuspemesanan'=>$statuspemesanan,'gambarlokasi'=>$row['gambar_lokasi']));
 }

 echo json_encode(array("result"=>$result));
 
 mysqli_close($con);