<?php
 require_once('dbConnect.php');

 $result = array();
 
 $sql = "SELECT * FROM lokasi";
 
 $res = mysqli_query($con,$sql);
 
 while($row = mysqli_fetch_array($res)){
 array_push($result,array('idlokasi'=>$row['id_lokasi'],'namalokasi'=>$row['nama_lokasi'],'latitude'=>$row['latitude'],'longitude'=>$row['longitude'],'gambar'=>$row['gambar_lokasi']));
 }
 
 echo json_encode(array("result"=>$result));
 
 mysqli_close($con);