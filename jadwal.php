<?php
 require_once('dbConnect.php');

 $namalokasi = $_GET['namalokasi'];

 $sql = "SELECT * FROM lokasi WHERE nama_lokasi='$namalokasi'";

 $res = mysqli_query($con,$sql);

 while($row = mysqli_fetch_array($res)){
 	$idlokasi = $row['id_lokasi'];

 }

 $result = array();

 $sql = "SELECT *,DATE(tanggal_pemesanan) as tanggal_pemesanan,DATE(tanggal_akhirPemesanan) as tanggal_akhirPemesanan FROM pemesanan inner join user on pemesanan.id_pelanggan = user.id_user WHERE id_lokasi='$idlokasi'";

 $res = mysqli_query($con,$sql);

 while($row = mysqli_fetch_array($res)){
 	$tanggalawal = strtotime($row['tanggal_pemesanan']);
  	$tanggalakhir = strtotime($row['tanggal_akhirPemesanan']);
	$timeDiff = abs($tanggalakhir - $tanggalawal);

	$numberDays = $timeDiff/86400;

	$numberDays = intval($numberDays);

 	if($numberDays!=0){
		for($i=0;$i<$numberDays+1;$i++){
			array_push($result,array('nama'=>$row['nama'],'tanggal'=>date('Y-m-d', strtotime('+'.$i.' day', $tanggalawal)),'tanggalakhir'=>$row['tanggal_akhirPemesanan']));
		}
	}
	else{
 		array_push($result,array('nama'=>$row['nama'],'tanggal'=>$row['tanggal_pemesanan'],'tanggalakhir'=>$row['tanggal_akhirPemesanan']));
	}

 }

 echo json_encode(array("result"=>$result));

 mysqli_close($con);
